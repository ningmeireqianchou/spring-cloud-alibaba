package com.lsh.test;

import com.lsh.test.feign.AccountApi;
import com.lsh.test.feign.OrderApi;
import com.lsh.test.feign.StorageApi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/18 5:10 下午
 * @desc ：Test服务启动类 9999
 */
@SpringBootApplication
//@ComponentScan("com.lsh.*")
@EnableDiscoveryClient
@EnableFeignClients(clients = {AccountApi.class, OrderApi.class, StorageApi.class})
public class TestApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class);
    }

}
