package com.lsh.test.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.lsh.test.feign.AccountApi;
import com.lsh.test.feign.OrderApi;
import com.lsh.test.feign.StorageApi;
import com.lsh.common.util.ResultObject;
import com.lsh.common.util.StatusCode;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/30 12:35 下午
 * @desc ：
 */
@RefreshScope
@RestController
@RequestMapping("/test")
public class TestController {
    @Value("${name:default}")
    public String name;
    @GlobalTransactional(name = "TEST-SEATA",rollbackFor = Exception.class)
    @SentinelResource("TEST-GETNAME")
    @GetMapping("/getName")
    public String getName(){
        return name;
    }

    @Autowired
    AccountApi accountApi;
    @Autowired
    OrderApi orderApi;
    @Autowired
    StorageApi storageApi;

    @GetMapping("order/update")
    public ResultObject updateOrder(){
        orderApi.update(1,1,0);
        return new ResultObject(true, StatusCode.OK,null);
    }

    @GetMapping("storage/decreaseStorage")
    public ResultObject decreaseStorage(){
        storageApi.decreaseStorage(1,1);
        return new ResultObject(true, StatusCode.OK,null);
    }
}
