package com.lsh.security.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 7:54 下午
 * @desc ：
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private String username;
    private String password;
    private String role;
    private String permission;

}
