package com.lsh.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 10:59 上午
 * @desc ：
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //默认URL根路径跳转到/login 此路径由SpringSecurity提供
        registry.addViewController("/").setViewName("redirect:/login");
//        registry.addViewController("/").setViewName("redirect:/login-view");
//        registry.addViewController("/login-view").setViewName("login");
    }
}
