package com.lsh.security;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 2:30 下午
 * @desc ：
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class BcryptPasswordEncoderTest {
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Test
    public void test(){
        String encode1 = bCryptPasswordEncoder.encode("123");
        String encode2 = bCryptPasswordEncoder.encode("123");
        System.out.println(encode1);// $2a$10$FFYhLLG2LWqkIHo0KgeRbuO1xt93cFn7Zce0TzmQY545uJcEL6Hgu
        System.out.println(encode2);// $2a$10$vR9G9qyFJtG.7u2fWIzpKeA8iozmgctMiGCjYfUR1Jtp7qVIaZpmK
        boolean matches1 = bCryptPasswordEncoder.matches("123", encode1);
        boolean matches2 = bCryptPasswordEncoder.matches("123", encode2);
        System.out.println("matches1: "+matches1);
        System.out.println("matches2: "+matches2);

    }
    @Test
    public void encode(){

        String encode1 = bCryptPasswordEncoder.encode("account");

        System.out.println(encode1);

    }
    @Test
    public void test1(){
        //使用BCrypt 加盐 加密
        String hashpw1 = BCrypt.hashpw("123", BCrypt.gensalt());
        String hashpw2 = BCrypt.hashpw("123", BCrypt.gensalt());
        System.out.println("hashpw1 : "+hashpw1);// $2a$10$/HwC6WiArmdFTER.nc0UquhE0CN7cZHy3ermoB7zc522kQyWbYTqq
        System.out.println("hashpw2 : "+hashpw2);// $2a$10$Sp7roG1rc7jQZxHu0Qw0GOXqBKCcYQc8m6kQlmEJg2cu1s4YwkyyK
        boolean checkpw1 = BCrypt.checkpw("123", hashpw1);
        boolean checkpw2 = BCrypt.checkpw("123", hashpw2);
        System.out.println("checkpw1 : "+checkpw1);
        System.out.println("checkpw2 : "+checkpw2);

    }
}
