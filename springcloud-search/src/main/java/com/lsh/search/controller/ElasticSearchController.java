package com.lsh.search.controller;

import com.lsh.common.util.ResultObject;
import com.lsh.common.util.StatusCode;
import com.lsh.search.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/31 4:27 下午
 * @desc ：
 */
@RestController
@RequestMapping("/elasticsearch")
public class ElasticSearchController {
    /**
     * ElasticsearchTemplate进行操作，
     * 7.X新版本主要使用 ElasticsearchRestTemplate
     */
    @Autowired
    ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    ElasticsearchRestTemplate elasticsearchRestTemplate;

    /**
     * 创建索引和映射
     * @return
     */
    @GetMapping("/createIndex")
    public ResultObject createIndex(){
        // 创建索引，会根据Item类的@Document注解信息来创建
        boolean index = elasticsearchTemplate.createIndex(Item.class);
        // 配置映射，会根据Item类中的id、Field等字段来自动完成映射
        boolean b = elasticsearchTemplate.putMapping(Item.class);
        return new ResultObject(true, StatusCode.OK,"创建Index:"+index+",配置映射："+b);

    }
}
