package com.lsh.order;

import com.lsh.order.handler.RtErrorHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/18 5:10 下午
 * @desc ：订单模块服务 启动类
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan("com.lsh.order.*")
@EnableFeignClients(basePackages = "com.lsh.order.feign")
//@EnableFeignClients(clients = {AccountApi.class, OrderApi.class, StorageApi.class})
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class);
    }


    @Bean
    @LoadBalanced // ribbon注解负载均衡
    public RestTemplate getRestTemplate(){
//        return new RestTemplate();

        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        // 解决401报错时，报java.net.HttpRetryException: cannot retry due to server authentication, in streaming mode
        requestFactory.setOutputStreaming(false);
        RestTemplate restTemplate = new RestTemplate(requestFactory);

        restTemplate.setErrorHandler(new RtErrorHandler());
        return restTemplate;
    }
}
