package com.lsh.order.repository;

import com.lsh.order.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/29 2:22 下午
 * @desc ：
 */
@Repository
public interface OrderRepository extends JpaRepository<Order,Integer> {

//    @Modifying
//    @Query(value = "UPDATE `goods_order` SET `money` = money - :money,`order_status` = 1 where user_id = :userId and `order_status` = :orderStatus",nativeQuery = true)
//    void updateOrderState(@Param("userId") Integer userId, @Param("money") Integer money, @Param("orderStatus") Integer orderStatus);

    Order findTopByUserId (Integer userID);
}
