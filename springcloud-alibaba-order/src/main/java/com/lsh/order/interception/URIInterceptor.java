package com.lsh.order.interception;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/7/19 2:21 下午
 * @desc ：拦截器 打印请求路径
 */
@Component
public class URIInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("URIInterceptor - RequestURL："+request.getRequestURL());
        String jsonToken = request.getHeader("username");
        if (jsonToken != null && !"".equals(jsonToken)){
            JSONObject object = JSON.parseObject(jsonToken);
            System.out.println("URIInterceptor:username:"+object.toString());
        }
        return true;
    }
}
