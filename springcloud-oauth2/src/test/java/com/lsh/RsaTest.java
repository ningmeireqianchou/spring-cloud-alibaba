package com.lsh;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/23 11:34 下午
 * @desc ：
 */
@SpringBootTest
public class RsaTest {
    @Test
    public void test1(){
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsib3JkZXIiXSwidXNlcl9uYW1lIjoienMiLCJzY29wZSI6WyJST0xFX0FETUlOIiwiUk9MRV9VU0VSIiwiUk9MRV9BUEkiLCJBTEwiXSwiZXhwIjoxNjI5NzM5ODkxLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sImp0aSI6IjQxYmJhZWI0LTZjNDMtNDFmNC04OWI1LWIzNTI5NTRiYmFhMSIsImNsaWVudF9pZCI6ImMxIn0.FsboID7aW42ILAKeyAHU_VU27LotBv9gU0935NBQ5_SqmEjeBqaqY2JnfzVo4EG2JX14aYi3pZhNLYMjMqly-6CNLtTCjPZIGcsUhebD3Lcsv7JhGuS6ddOcoLByIuUlvTUAAM56LdGHQjvC_Za25dE2fMeubRqZofpQMZt4hwC5e0YVtZ_s8oGGhSZfqVcbjzZ-_odgF9yz_wAHPr-PXF3iR9Ba1ctqDinKzD8szN-T1daFFsGccRbib-Lncm0DPx8DuvgsUgDJ9b6DOiCyWASQATl7EppJi1T1Nn2bqa0WVBxphGq2bUq3PkRurZ0TqxyAfSDawOKTtrRmqec7aw";

        String publickey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG8w0BAQEFAAOCAQ8AMIIBCgKCAQEAp7vbquG9Aerqxc1qOOauGvGxYAUwn/lXrGlwQigKxR44XsKv57dEFGxcKjxirvbubZEbW3sPlWxQFrx7DCr/paJ8h7o041/XdCOSQ3HWwwG2WHPzn5VSqnFatuWfkkd1P61n/uu9V2wQaBIiSLhZmKaqFgwo1IAYTlPvxLyi6Kkh1yCqSzQPy1cP6Kv1iAdMm/b6fCIekw3SU1ztD2X3s67tb683XS95Nli1KW1436/DYR0YQy3LnIGjS/nb0/5zz6FyqSnYOVbKcL3FMtiQZI9K4xQD3l1Pf68RE4y9cVOg9D5lMD4e+emA5DXYishZrT2f99XeCq4nXUYH9wVVTQIDAQAB-----END PUBLIC KEY-----";
        //校验Jwt
//        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publickey));

        Jwt jwt = JwtHelper.decode(token);
        //获取Jwt原始内容 载荷
        String claims = jwt.getClaims();
        System.out.println(claims);
    }
    @Test
    public void test2(){
        int a,b;
        //8 << 3; ==> 8*2*2*2
        a = 8 << 1;
        // 8 >> 1  ==> 8/2
        b = 8 >> 1;
        System.out.println("a:"+a);
        System.out.println("b:"+b);

    }
}
