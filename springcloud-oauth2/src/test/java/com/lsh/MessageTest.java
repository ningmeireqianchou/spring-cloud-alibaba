package com.lsh;

import com.lsh.oauth2.SpringSecurityOAuth2Application;
import com.lsh.oauth2.util.MessageUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/22 10:37 上午
 * @desc ：
 */
@SpringBootTest(classes= SpringSecurityOAuth2Application.class)
@RunWith(SpringRunner.class)
public class MessageTest {
    @Test
    public void test (){
        //params look like "{0}", "{1,date}", "{2,time}" within a message
        String message = MessageUtils.message("user.password.retry.limit.exceed","5","10");
        System.out.println(message);
    }
}
