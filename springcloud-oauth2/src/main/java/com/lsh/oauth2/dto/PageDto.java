package com.lsh.oauth2.dto;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/17 2:18 下午
 * @desc ：
 */
@Data
public class PageDto {
    private Integer pageSize =10;
    private Integer pageNum =1;
}
