package com.lsh.oauth2.controller;

import com.lsh.common.annotation.Log;
import com.lsh.common.constant.HttpStatus;
import com.lsh.common.enums.BusinessType;
import com.lsh.common.enums.OperatorType;
import com.lsh.common.util.AjaxResult;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/24 10:03 上午
 * @desc ：
 */
@RestController
public class TestController {
    /**
     * @Log注解
     * @title 模块名
     * @businessType 操作类型： BusinessType.class 枚举类型
     * @operatorType  操作人类别：OperatorType.class 枚举类型 手机端用户、后台用户、其他
     * @isSaveRequestData  是否保存请求的参数
     *
     * @return
     */
    @GetMapping("/oauth2/test/get/{name}/{id}")
    @Log(title = "认证模块",businessType = BusinessType.INSERT,operatorType = OperatorType.MANAGE,isSaveRequestData = true)
    public AjaxResult get(@PathVariable("name")String name, @PathVariable("id") Integer id){
        return new AjaxResult(HttpStatus.SUCCESS,"操作成功！",new Date().toString());
    }
    @PostMapping("/oauth2/test/post")
    @Log(title = "认证模块",businessType = BusinessType.INSERT,operatorType = OperatorType.MANAGE,isSaveRequestData = true)
    public AjaxResult post(@RequestBody Map map){
        return new AjaxResult(HttpStatus.SUCCESS,"操作成功！",new Date().toString());
    }

}
