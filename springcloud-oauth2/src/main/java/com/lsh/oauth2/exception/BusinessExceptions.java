package com.lsh.oauth2.exception;

import com.lsh.common.util.StringUtils;
import com.lsh.oauth2.util.MessageUtils;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/18 1:49 下午
 * @desc ： 自定义业务异常  继承RuntimeException运行时异常
 */
public class BusinessExceptions extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * 所属模块
     */
    private String module;

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误码对应的参数
     */
    private Object[] args;

    /**
     * 错误消息
     */
    private String defaultMessage;

    /**
     * 全参构造
     * @param module 模块
     * @param code 错误码
     * @param args 参数
     * @param defaultMessage 错误信息
     */
    public BusinessExceptions(String module, String code, Object[] args, String defaultMessage) {
        this.module = module;
        this.code = code;
        this.args = args;
        this.defaultMessage = defaultMessage;
    }

    public BusinessExceptions(String module, String code, Object[] args) {
        this(module, code, args, null);
    }

    public BusinessExceptions(String module, String defaultMessage) {
        this(module, null, null, defaultMessage);
    }

    public BusinessExceptions(String code, Object[] args) {
        this(null, code, args, null);
    }

    public BusinessExceptions(String defaultMessage) {
        this(null, null, null, defaultMessage);
    }

    public BusinessExceptions(){}


    @Override
    public String getMessage() {
        String message = null;
        if (!StringUtils.isEmpty(code)) {
            message = MessageUtils.message(code, args);
        }
        if (message == null) {
            message = defaultMessage;
        }
        return message;
    }

    public String getModule()
    {
        return module;
    }

    public String getCode()
    {
        return code;
    }

    public Object[] getArgs()
    {
        return args;
    }

    public String getDefaultMessage()
    {
        return defaultMessage;
    }
}
