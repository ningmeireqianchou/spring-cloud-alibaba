package com.lsh.oauth2.repository;

import com.lsh.oauth2.entity.SysRolePermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 8:20 下午
 * @desc ：角色-权限关联表
 */
public interface SysRolePermissionRepository extends JpaRepository<SysRolePermission,Integer> {

    //根据角色ID查询对应权限ID
    @Query(value = "SELECT permission_id FROM  sys_role_permission WHERE role_id IN (:roleIds)",nativeQuery = true)
    List<Integer> findPermissionIdByRoleIds(List<Integer> roleIds);
}
