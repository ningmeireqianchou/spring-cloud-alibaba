package com.lsh.oauth2.repository;

import com.lsh.oauth2.entity.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 8:06 下午
 * @desc ：
 */
public interface SysUserRepository extends JpaRepository<SysUser,Integer> {
    // 根据用户名查询用户信息
    SysUser findTopByUserName(String username);
}
