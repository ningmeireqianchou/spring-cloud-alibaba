package com.lsh.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/19 11:00 下午
 * @desc ：
 */
@SpringBootApplication
@EnableHystrix
@EnableDiscoveryClient
//@EnableFeignClients(basePackages = {"com.lsh."})
public class SpringSecurityOAuth2Application {
    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityOAuth2Application.class,args);
    }
}
