package com.lsh.oauth2.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/18 7:53 下午
 * @desc ：
 */
@NoArgsConstructor
@Data
@Entity
@Table(name = "sys_user")
public class SysUser implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String userName;
    private String password;

    public SysUser(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }
}
