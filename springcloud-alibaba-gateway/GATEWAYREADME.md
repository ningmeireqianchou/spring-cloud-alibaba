# 请求转发

## Account 服务
http://localhost:9004/account/gateway/test   ==》  http://localhost:9001/account/gateway/test
## Order服务
http://localhost:9004/order/gateway/test     ==》  http://localhost:9002/order/gateway/test
## Storage服务
http://localhost:9004/storage/gateway/test   ==》  http://localhost:9003/storage/gateway/test

# 网关 = 路由转发+过滤器
路由转发：
通过网关的路由转发，转发到相应的后端服务上

过滤器：
网关的容错、限流、以及相应的额外处理

# SpringCloud 中提供的网关解决方案：

Zuul:
简单灵活   Netflix
Gateway：
Spring产品
提供微服务网关功能，包含：权限校验、监控/指标、谓词（断言）、过滤器（限流）、容错处理

# 注意：
如果项目中加入了server.servlet.context-path （项目名），需要访问gateway/服务名称/项目名/接口

# Route（路由）
一个路由包含了ID、URI、Pridicate集合、Filter集合，
ID
    在Route中ID是自定的
URI
    地址
Predicate（谓词）
    附加条件，路由规则、转发规则

Filter（过滤）


HandlerMapping -> Web Handler(若干过滤器)


# Gateway 动态路由
Spring Cloud Gateway 提供了 Endpoint 端点，暴露路由信息，
有获取所有路由、刷新路由、查看单个路由、删除路由等方法，
源码在 org.springframework.cloud.gateway.actuate.GatewayControllerEndpoint 中，
想访问端点中的方法需要添加 spring-boot-starter-actuator 注解，并在配置文件中暴露所有端点
1.根据Spring Cloud Gateway的路由模型定义数据传输模型，分别是：路由模型、过滤器模型、断言模型
2.编写动态路由实现类，需实现ApplicationEventPublisherAware接口
3.编写 Rest接口，通过这些接口实现动态路由功能，注意SpringCloudGateway使用的是WebFlux不要引用WebMvc
4.查看网关路由信息，访问 localhost:9004/actuator/gateway/routes
## 1.添加路由
```
{
    "id": "routeadd",
    "predicates": [
        {
            "name": "Path",
            "args": {
                "pattern": "/ccc/**"
            }
        }
    ],
    "filters": [
        {
            "name": "StripPrefix",
            "args": {
                "parts": "1"
            }
        }
    ],
    "uri": "https://www.baidu.com",
    "order": 1
}
```
# 网关接入认证授权服务
网关也相当与一个资源服务
```
          # oauth2认证授权服务
        - id: oauth2
          uri: lb://springboot-security-oauth2
          predicates:
            - Path=/oauth2/**
```








