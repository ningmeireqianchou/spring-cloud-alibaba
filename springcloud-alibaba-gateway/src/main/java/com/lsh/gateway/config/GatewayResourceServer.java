//package com.lsh.gateway.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.Customizer;
//import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
//import org.springframework.security.config.web.server.ServerHttpSecurity;
//import org.springframework.security.oauth2.jwt.NimbusReactiveJwtDecoder;
//import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
//import org.springframework.security.oauth2.jwt.ReactiveJwtDecoders;
//import org.springframework.security.web.server.SecurityWebFilterChain;
//
///**
// * @author ：LiuShihao
// * @date ：Created in 2021/8/25 12:34 下午
// * @desc ：将网关配置为OAuth2的一个资源服务
// *官方文档：
// * https://docs.spring.io/spring-security/site/docs/5.3.2.RELEASE/reference/html5/#reactive-applications
// */
//@Configuration
//@EnableWebFluxSecurity
//public class GatewayResourceServer {
//
//    /**
//     * 第一个是SecurityWebFilterChain将应用程序配置为资源服务器的。
//     * 当包含时spring-security-oauth2-jose，
//     * 这看起来像：WebSecurityConfigurerAdapter
//     * @param http
//     * @return
//     */
//    @Bean
//    SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
//        http
//                .authorizeExchange(exchanges -> exchanges
//                        //资源服务器将尝试将这些范围强制转换为授予权限的列表，并在每个范围前添加字符串“SCOPE_”。
//                        .pathMatchers("/message/**").hasAuthority("SCOPE_message:read")
//                        .pathMatchers("/contacts/**").hasAuthority("SCOPE_contacts")
//                        .anyExchange().authenticated()
//                )
//                .oauth2ResourceServer(oauth2 -> oauth2
//                        .jwt(withDefaults())
//                );
//        return http.build();
//    }
//
//
//    /**
//     * 第二个@BeanSpring Boot 创建的是 ReactiveJwtDecoder它将String令牌解码为经过验证的实例Jwt：
//     * @return
//     */
//    @Bean
//    public ReactiveJwtDecoder jwtDecoder() {
//        return ReactiveJwtDecoders.fromIssuerLocation(issuerUri);
//    }
//
//    public String secretKey ="secretKey";
//    /**
//     * 使用对称秘钥
//     * @return
//     */
//    @Bean
//    public ReactiveJwtDecoder jwtDecoder() {
//        return NimbusReactiveJwtDecoder.withSecretKey(secretKey).build();
//    }
//
//}
