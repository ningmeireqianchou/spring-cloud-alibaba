package com.lsh.gateway.dto;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/17 10:48 上午
 * @desc ：
 */
@Data
public class GatewayFilterDefinition {

    //Filter Name
    private String name;
    //对应的路由规则
    private Map<String, String> args = new LinkedHashMap<>();
}
