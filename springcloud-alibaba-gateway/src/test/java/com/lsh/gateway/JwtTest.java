package com.lsh.gateway;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/8/25 3:08 下午
 * @desc ：
 */
@SpringBootTest
public class JwtTest {

    //证书文件路径
    String key_location="rsa_first.jks";
    //秘钥库密码
    String key_password="rsapassword";
    //秘钥密码
    String keypwd = "rsapassword";
    //秘钥别名
    String alias = "rsafirst";


    @Test
    public void test1(){

        //访问证书路径 读取jks的文件
        ClassPathResource resource = new ClassPathResource(key_location);
        //创建秘钥工厂  加载读取证书数据
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(resource,key_password.toCharArray());
        //读取秘钥对(公钥、私钥)  获取证书中 的一对秘钥   （别名，密码）
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair(alias,keypwd.toCharArray());
        //获取私钥
        RSAPrivateKey rsaPrivate = (RSAPrivateKey) keyPair.getPrivate();
        //生成Jwt令牌   JwtHelper是SpringSecurity提供的类   （载荷，盐（私钥 ））
        String msg ="{\n" +
                "    \"aud\": [\n" +
                "        \"order\"\n" +
                "    ],\n" +
                "    \"user_name\": \"zs\",\n" +
                "    \"scope\": [\n" +
                "        \"ROLE_ADMIN\",\n" +
                "        \"ROLE_USER\",\n" +
                "        \"ROLE_API\",\n" +
                "        \"ALL\"\n" +
                "    ],\n" +
                "    \"user_age\": 18,\n" +
                "    \"active\": true,\n" +
                "    \"exp\": 1629878278,\n" +
                "    \"authorities\": [\n" +
                "        \"ROLE_ADMIN\"\n" +
                "    ],\n" +
                "    \"jti\": \"6b958697-cfa0-41ff-925f-f440058b5b9d\",\n" +
                "    \"client_id\": \"c1\"\n" +
                "}";
        Jwt jwt = JwtHelper.encode(msg, new RsaSigner(rsaPrivate));
        System.out.println(jwt);
        String encoded = jwt.getEncoded();
        System.out.println(encoded);



    }

    @Test
    public void test2(){
        String jwtToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsib3JkZXIiXSwidXNlcl9uYW1lIjoienMiLCJzY29wZSI6WyJST0xFX0FETUlOIiwiUk9MRV9VU0VSIiwiUk9MRV9BUEkiLCJBTEwiXSwidXNlcl9hZ2UiOjE4LCJleHAiOjE2Mjk4NzgyNzgsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwianRpIjoiNmI5NTg2OTctY2ZhMC00MWZmLTkyNWYtZjQ0MDA1OGI1YjlkIiwiY2xpZW50X2lkIjoiYzEifQ.FxUGh7VTfSz2qQsjjXElN1VFyVIepF0CZrjLfvCeG-IOE8pCn8v8g2NLtnYj_csjpknxpfBzlyFImc05jErJacdYXzTnPh99QfauKoLy1WrEMmVjAUWd0KniLxIe67MomLcnbb7B9_7G8FuEBPC8LKCrFWRTlGCw59r5EcIUz3zlBtH49iPG7ALpskKvqLR8gQudSEJGJyu7dnab2eslxc3Ddp06NX50wyLSq9sPfSB5ImHfa12JP2NOUg5NMXxzXUxqyZBOlrsSSXexv4fVRBInLlWKtsyy-k4Q7DTJXSxDgz-DQkkEvfuaUIhUBu56REQQ06eLaJTqYWfGeOYquw";
        String publicKey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp7vbquG9Aerqxc1qOOauGvGxYAUwn/lXrGlwQigKxR44XsKv57dEFGxcKjxirvbubZEbW3sPlWxQFrx7DCr/paJ8h7o041/XdCOSQ3HWwwG2WHPzn5VSqnFatuWfkkd1P61n/uu9V2wQaBIiSLhZmKaqFgwo1IAYTlPvxLyi6Kkh1yCqSzQPy1cP6Kv1iAdMm/b6fCIekw3SU1ztD2X3s67tb683XS95Nli1KW1436/DYR0YQy3LnIGjS/nb0/5zz6FyqSnYOVbKcL3FMtiQZI9K4xQD3l1Pf68RE4y9cVOg9D5lMD4e+emA5DXYishZrT2f99XeCq4nXUYH9wVVTQIDAQAB-----END PUBLIC KEY-----";
        //校验Jwt
        Jwt jwt = JwtHelper.decodeAndVerify(jwtToken, new RsaVerifier(publicKey));

        //获取Jwt原始内容 载荷
        String claims = jwt.getClaims();
        System.out.println(claims);

    }

    @Test
    public void test3(){
        String token = "Bearer token";
        String substring = token.substring(7);
        System.out.println(substring);
    }

}
