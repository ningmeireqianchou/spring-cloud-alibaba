package com.lsh.common.annotation;

import com.lsh.common.enums.BusinessType;
import com.lsh.common.enums.OperatorType;

import java.lang.annotation.*;

/**
 * @desc 自定义操作日志记录注解
 * @author LiuShihao
 * @date 2021年09月24日09:16:35
 *
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
    /**
     * 模块 
     */
    public String title() default "";

    /**
     * 功能
     */
    public BusinessType businessType() default BusinessType.OTHER;

    /**
     * 操作人类别
     */
    public OperatorType operatorType() default OperatorType.MANAGE;

    /**
     * 是否保存请求的参数
     */
    public boolean isSaveRequestData() default true;
}
