package com.lsh.user.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/17 9:27 上午
 * @desc ：
 */
@Data
@Component
@ConfigurationProperties(prefix = "oauth2")
public class Oauth2Configuraiton {

    private String login;

    private String token;
}
