package com.lsh.user;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/15 10:26 上午
 * @desc ：
 */
@SpringBootTest
public class CaptchaText {
    @Test
    public void test() throws IOException {
        String captche = "/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAA8AKADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDU8L+GNAuPCejTTaHpkksljA7u9pGWZiikkkjkmtceEfDf/QvaT/4BR/4UnhH/AJE3Q/8AsH2//ota2xQBkDwj4a/6F7Sf/AKP/wCJpw8IeGv+hd0n/wAAo/8A4mtgU4UAZA8IeGf+hd0j/wAAo/8A4mnDwf4Y/wChc0j/AMAY/wD4mn674h07w5pz3uoziONQdq/xOfQD1rA8KfE7R/Fd+LC3gube6KFwsqjBx1wQaAN4eDvDH/QuaR/4Axf/ABNPHg7wv/0Lej/+AMX/AMTWwvNPFAGMPBvhf/oW9H/8AYv/AImnjwZ4W/6FrR//AABi/wDia2RTxQBijwZ4W/6FrR//AABi/wDiaePBfhX/AKFrRv8AwAi/+JraFOFAGKPBXhX/AKFnRv8AwAi/+Jpw8FeFP+hZ0b/wAi/+JrbFR3N3b2Vu9xczRwwoMs8jAAfiaAMseCfCn/QsaL/4ARf/ABNOHgjwn/0LGi/+AEX/AMTT9D8UaP4ie6XSr1Ln7MwWVk6Anp/I1tigDEHgjwn/ANCvov8A4L4v/iacPA/hL/oV9E/8F8X/AMTW4KeKAMIeB/CX/QraJ/4L4v8A4msfxj4N8L2vgfxBcW/hvR4Z4tNuHjkjsYlZGETEEELkEHvXcCsPxx/yT7xJ/wBgq6/9FNQByXhH/kTNC/7B9v8A+i1rbFYvhH/kTNC/7B9v/wCi1rbFADhSnhaBWdqOv6NpakX+qWdsR/DLMqt+Wc0AeYfGfy59LtZHkZZIZcIuCQ2RyPY8Z/CuP8DeKdE0Sa3OoWbRXETHF5GuTtPZsc/kK2viJ4t8P6xPZW9pdG7gSYyT+XGw4CnAG7Gck9q85vTBdTF7G0kjQD5h1H19qAPrfStQttRsIbu1mSaCVdyOhyCKvJLG7MqupK8MAelfK3hjUfFEtrcaTod+YLd/nkTzAu3tkZ5Hviq+p6NqWkXsTareS/Z7ghZbiBmk4PUHOMn29qAPrOKeGYMYpUfa21trA4Pofele6t4gTJNGoHXcwFfOepeFPBWn6XCrazerPdx+ZbXTgNAx9wq5Hvk1yMejxabrtlBrLL9ilYMZYX3KyHuD6UAfWMPiDRpgTHqtk4HXFwpx+tObxDosY+fV7BfrcoP6187+Jvh3bIpudBlbAGTbTNnP+639D+dc54d07TdSkutK1JVs7v8A5Y3BZgyuDyhUnB/LNAH1VB4o0G6mENtrWnzzEEiOK6R2OBk4AOelcr4p8Y+Fb7TbvS7rVbUpMhjdC/I9/wCor54s9Og0/wAWJYXvlzRLJtO8Ha4PTODkZ9ual8YaLFpWql7RCtnLyi7i2w9xk8mgDZ8E+KR4A8YSH7QtzpVxiOZ4juBXOVcY7jJ49Ce9fU+n3kN9aRXEEqSxSoHR0OQykZBB7ivkey8KQanZxz2106qy9WUNg9xxitvwj488S+B7saB5tkLYy4X7eHMcOe4ZTkKevQjvgc0AfVQp4rg4fEfjuBAZ/BtpfL1Mljqir+SuBn86mHxDntuNS8GeJrYjq8dmJ0H/AAJGP8qAO5FYfjj/AJJ94l/7BV1/6KatPTb6LU9Ot76BZVinQOqyoUYD3U8g1meOf+SfeJf+wVdf+imoA5Pwj/yJmhf9g+3/APRa1c1mK/l0S9TSphDfmFvs7kAgPjjqCKqeEP8AkTNC/wCwfb/+i1rbFAHkg+G3jbWv+Q74uZI26xxSPIP++flWrR+EHhPQtPm1HWtQvJ4bdC8rFxGmB7AZz2xmu517xZovhm2eXUr6NHUAiBSGlfPTC9ecdenvXIacniD4h6raX+p2K2HheCTzo7OXO+6I+6WHcZweePr1oAPBHw+0t4pdcvNHih+182dlKDJ9nh/hLbs5dupJ6dsdK0PEXhuFYD5USqoHAAwK78CqGqwCW2YY7UAfL0BbRfGqpASFS427R3Unp+Rr1XWdCS606WKVd0ci8+x7Gsv/AIQO3Hij+0C07P53m7WIK7s59OlelSacZdN2lecUAfMk91Mtm+kSkusU+Y8/wnkH8+Kju3mhjFlI25Y23Rk/w56j/PpXpWoeAozq73KRPuZ92M8Z9afD4EWXUFnmg8w8cMMj8qAOl8M6dLqHhSxkkz5vkjOa4rxv4XIV7yNNtxHySP4h/jXrkd/pnhTQxd6vcpbW4IQEgkk9gAOSeD09K82+IPxA0S+sTBocpuJpRgyGNlVB/wACAyaAPNLBbrVdZtkJLyllG7vgetepeIvDZvtIZSuW25U+hrD+GWivdTNdNbkAthZW/i+ntXuM+hCXT9u3nFAHzPoHiC48NXckM0Pm25OJIicEH1Bo8Va3Z61cQS2sToUUglxz9K9T1TwUjXDs9nHJnuyZrnNQ8CpcBYktzAobOYgAf5UAep/BnXZ9T8EWkdy5aS2ZoAx6lVPy/kMD8K9QXkV5r8OdEGiaXDZxhhGmTlupJOSTXpSD5RQA8VheOf8AknviX/sFXX/opq3hWF45/wCSe+Jf+wVdf+imoA5Pwh/yJehf9g63/wDRa1e1e9n07R7u8trOS8nhjLJbxfekPYf55+teCab8YvEOl6ZaafBZ6Y0VrCkKF4pCxVVCjOHHOBVr/hePib/nx0j/AL8yf/HKAOm+Hfg+fXL+bxl4oi8+5uHL20My8D/bKnsOijsBn0r10V8+/wDC8/E3/PjpH/fmT/45S/8AC9PE/wDz4aR/35l/+OUAfQgoeMOuDXz5/wAL28T/APPhpH/fmX/45S/8L38Uf8+Gj/8AfmX/AOOUAe7DS4vM37Rmry267NuOK+fP+F8+KP8Anw0f/vzL/wDHKX/hfXin/nw0f/vzL/8AHKAPem0qFn3FBT10uFeQgrwP/hfnin/nw0b/AL8y/wDxyl/4X74q/wCgfo3/AH5l/wDjlAHq/i/Q7HV7EW99aLOiHKZ6qfUHtXmcPgHToLvdDZM3PAkYsB+BrOm+OfiWcYfTtGP/AGxl/wDjlV1+M+vo24aZo+f+uMn/AMcoA9m8LaCbZVZkx+Fd2kQ2BcV81x/HvxREMJp2ij/tjL/8cqUftCeLB/zD9F/78y//ABygD6Jk06GTkoKrPoVuxzsH5V4D/wANC+Lf+gdon/fmX/45R/w0P4t/6B2if9+Jf/jlAH0baWMduAFUCr4FfMn/AA0R4uH/ADDtE/78S/8Ax2nf8NFeLv8AoHaJ/wB+Jf8A47QB9OCsLxz/AMk98S/9gq6/9FNXgP8Aw0X4v/6B2h/9+Jf/AI7VTVvj34p1jRr7S7iw0ZYLy3kt5GjhlDBXUqSMyEZwfQ0Af//Z";
        //使用RY的工具类解码可以
        //byte[] decode = Base64.decode(captche);
        //使用java.util.Base64解码也可以
        byte[] decode = Base64.getDecoder().decode(captche.getBytes());

        ByteArrayInputStream bis = new ByteArrayInputStream(decode);


        File file = new File("./captche.jpg");
        FileOutputStream fos = new FileOutputStream(file);
        byte[] bytes = new byte[bis.available()];
        while (bis.read(bytes)!= -1){
            fos.write(bytes);
        }
        bis.close();
        fos.close();
        System.out.println("===完成===");

    }
}
