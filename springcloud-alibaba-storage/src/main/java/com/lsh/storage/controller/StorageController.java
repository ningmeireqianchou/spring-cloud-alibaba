package com.lsh.storage.controller;

import com.lsh.storage.entity.Storage;
import com.lsh.storage.feign.AccountApi;
import com.lsh.storage.service.StorageService;
import com.lsh.common.util.ResultObject;
import com.lsh.common.util.StatusCode;
import com.lsh.common.util.SystemConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/6/29 2:32 下午
 * @desc ：
 */
@Slf4j
@RestController
@RequestMapping("/storage")
public class StorageController {
    @Autowired
    StorageService storageService;

    @Autowired
    AccountApi accountApi;



    @GetMapping("/decrease")
    public ResultObject decreaseStorage(@RequestParam("productId") Integer productId, @RequestParam("count") Integer count){
        log.info("STORAGE-扣减库存"+productId+" : "+count);
        storageService.decreaseStorage(productId,count);
        return new ResultObject(true, StatusCode.OK, SystemConstants.DECR_STORAGE_SUCCESS);
    }

//    @SentinelResource("sentinel-storage")
    @GetMapping("/findAll")
    public ResultObject findAll(){
        log.info("STORAGE-查询全部库存数据");
        List<Storage> all = storageService.findAll();
        return new ResultObject(true, StatusCode.OK, SystemConstants.QUERY_SUCCESS,all);
    }


    @GetMapping("/accountToStorage")
    public ResultObject accountToStorage(){

        String allStorage = accountApi.findAllStorage();

        return new ResultObject(true, StatusCode.OK, SystemConstants.QUERY_SUCCESS,allStorage);
    }
    @GetMapping("/findAllAccount")
    public ResultObject findAllAccount(){

        String all = accountApi.findAll();

        return new ResultObject(true, StatusCode.OK, SystemConstants.QUERY_SUCCESS,all);
    }





}
