package com.lsh.account.test;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/6 10:16 上午
 * @desc ：
 *  主线程main的优先级是5。
 *  默认的优先级是5
 */
public class Tux extends Thread {
    static String sName = "vandeleur";

    public static void main(String[] args) {
        Thread.currentThread().getPriority();
        Tux t = new Tux();
        t.piggy(sName);
        System.out.println("3:"+sName);
    }

    public void piggy (String sName){
        sName = sName + "wiggy";
        System.out.println("1:"+sName);
        start();
    }
    public void run(){
        System.out.println("2:"+sName);
        for (int i = 0; i < 4; i++) {
            sName = sName + "" +i;
        }
    }
}
