package com.lsh.account.controller;

import com.lsh.account.feign.StorageApi;
import com.lsh.account.service.AccountService;
import com.lsh.common.util.ResultObject;
import com.lsh.common.util.StatusCode;
import com.lsh.common.util.SystemConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * @author LiuShihao
 * @Date 2021年06月29日14:05:28
 */
@Slf4j
@RestController
@RequestMapping("/account")
public class AccountController {
    @Value("${name}")
    private String name;

    @Autowired
    AccountService accountService;

    @Autowired
    StorageApi storageApi;


    /**
     * 扣减账户余额
     * @param userId 用户id
     * @param money 金额
     * @return
     */
//    @SentinelResource(value = "account-decrease")
    @GetMapping("/decrease")
    public ResultObject decreaseAccount(@RequestParam("userId") Integer userId, @RequestParam("money") Integer money){
        accountService.decrease(userId,money);
        return new ResultObject(true, StatusCode.OK, SystemConstants.DECR_ACCOUNT_SUCCESS);

    }
    /**
     * 查询所有账户数据
     * @RequestHeader("myheader")
     * @return
     */
    @GetMapping("/findAll")
    public ResultObject findAll(){
        return new ResultObject(true, StatusCode.OK, "9001-"+SystemConstants.QUERY_SUCCESS,accountService.findAll());
    }
    @GetMapping("/findAllByMyBatis")
    public ResultObject findAllByMyBatis( ){
        return new ResultObject(true, StatusCode.OK, "9001-"+SystemConstants.QUERY_SUCCESS,accountService.findAllByMyBatis());
    }
    @GetMapping("/findAllAndHeader")
    public ResultObject findAllAndHeader(@RequestHeader("myheader") String myheader,@RequestHeader("newheader") String newheader,String name,Integer age){
        System.out.println("myheader:"+myheader);
        System.out.println("newheader:"+newheader);
        System.out.println("name:"+name);
        System.out.println("age:"+age);
        return new ResultObject(true, StatusCode.OK, SystemConstants.QUERY_SUCCESS,myheader+","+newheader+","+name+","+age);
    }

    /**
     * Account服务查询所有库存数据
     * @return
     */
    @GetMapping("/findAllStorage")
    public ResultObject findAllStorage(){
        log.info("Account服务查询所有库存数据");
        Object allStorage = accountService.findAllStorage();
        return new ResultObject(true, StatusCode.OK, SystemConstants.QUERY_SUCCESS,allStorage);
    }


    @GetMapping("/getServiceName")
    public ResultObject getServiceName(){
        return new ResultObject(true, StatusCode.OK, SystemConstants.QUERY_SUCCESS,name);
    }

    /**
     * Account -- > Storage
     * Storage  -- Account
     * Account -- > Storage findAll
     * @return
     */
    @GetMapping("/reqStorageCallBackAccount")
    public ResultObject reqStorageCallBackAccount(){
        String s = storageApi.accountToStorage();
        return new ResultObject(true, StatusCode.OK, SystemConstants.QUERY_SUCCESS,s);
    }


}
