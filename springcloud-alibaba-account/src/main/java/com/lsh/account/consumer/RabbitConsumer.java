package com.lsh.account.consumer;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/6 12:28 下午
 * @desc ：
 */

@Component
public class RabbitConsumer {
    /**
     * 监听死信队列
     * @param message
     * @param channel
     * @throws IOException
     */
    @RabbitListener(queues = "deadLetterQueue")
    public void receiveA(Message message, Channel channel) throws IOException {
        String msg = new String(message.getBody());
        System.out.println(new Date().toString()+": deadLetterQueue死信队列收到消息："+msg);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
