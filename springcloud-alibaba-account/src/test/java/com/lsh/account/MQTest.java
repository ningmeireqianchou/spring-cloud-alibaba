package com.lsh.account;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/13 3:29 下午
 * @desc ：
 */
@SpringBootTest(classes = AccountApplication9000.class)
@RunWith(SpringRunner.class)
public class MQTest {
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Test
    public void test() throws IOException, TimeoutException {

        for (int i = 0; i < 20; i++) {
            rabbitTemplate.convertAndSend("direct2","哈哈哈今天周二啦！——"+i);
            System.out.println("已发送："+i);
        }

    }
}
